require 'terminal-table'
require 'json'

class Catalogue

  # Initialize with the list of products in the system
  def initialize
    raw_products = File.read("products.json")
    @products = JSON.parse(raw_products)
  end

  def products
    @products
  end

  def display_catalogue
    @items = []
    products.select { |product, code|
      @items << :separator
      code["packs"].select { |pack, price|
        @items << [code["name"], code["code"], pack, '$' + price.to_s]
      }
    }
    table = Terminal::Table.new :title => "Ruby's Bakery Menu", :rows => @items, :headings => ['Name of the Product', 'Code', 'Packs', 'Pack Price']
    table.align_column(2, :center)
    table.align_column(3, :right)
    puts table
  end

end