require_relative 'Catalogue'

class InputValidator
	def initialize(code, items, catalogue)
		@code = code
		@items = items
    @catalogue = catalogue
	end

  # Checks if the given items number is a valid integer
  def is_valid_number?
    true if (Integer(@items) && Integer(@items) >= 0) rescue false
  end

  # Check if the entered product by the user exists in the catalogue
  def check_existence
    @catalogue.products.each_key do | key |
      return true if @code == key
    end
    false
  end

  # Some errors to display to the user

  def product_error
    "\nThe product does not exist. Please try again."
  end

  def items_error
    "\nPlease enter a valid item number."
  end
end
