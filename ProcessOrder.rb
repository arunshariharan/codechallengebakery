require 'terminal-table'
require_relative 'InputValidator'
require_relative 'SelectionAlgorithm'
require_relative 'DisplayResult'

class ProcessOrder
  
  attr_accessor :order_code, :items, :catalogue, :packs, :prices, :line_item, :best_package, :error, :display

  def initialize(order_code, items, catalogue)
    @order_code = order_code
    @items = items
    @catalogue = catalogue
    @packs, @prices, @line_item = [], [], {}
    @display = DisplayResult.new
  end

  # Core method which is reponsible for executing all steps in order
  def execute
    if valid_inputs
      select_product 
      find_combinations
      finalize_orders
      check_package_status ? print_result : display_error
    else
      display_error
    end
  end

  # Select the product as requested by the user, from the catalogue
  # From the product, determine the available packages and prices
  def select_product
    catalogue.products.select do | product, code |
      if code["code"] == order_code
        code["packs"].each_key { |key| @packs << key.to_i }
        code["packs"].each_value { |value| @prices << value }
      end
    end
  end

  # Send the packs and required items to Selection Algorithm
  # To calculate the minimal package options required to satisfy the order
  def find_combinations
    selection_algorithm = SelectionAlgorithm.new(items.to_i, packs)
    @best_package = selection_algorithm.find_minimal_packaging
  end

  # if the value returned from the algorithm is not an error
  def check_package_status
    if best_package.class == Array
      return true
    else
      @error = best_package
      return false
    end
  end

  # Print the resulting package or any errors
  def finalize_orders
    if check_package_status
      # Send the total packs, number of items ordered and the price 
      # to be added to the order table
      catalogue.products.select { |product, code|
        if code["code"] == @order_code
          package_count.each { |pack|
            code["packs"].select { |quantity, price|
              if quantity == pack[0].to_s
                @line_item = @display.set_values(pack[0], pack[1], price, code["name"])
              end
            }
          }
        end
      }
    end
  end

  private 

  # Checks if the given inputs are valid and exist
  def valid_inputs
    @validator = InputValidator.new(order_code, items, catalogue)
    if @validator.check_existence && @validator.is_valid_number?
      return true
    elsif @validator.check_existence
      @error = @validator.items_error
    else
      @error = @validator.product_error
    end
    return false
  end

  def package_count
    count = Hash.new(0)    
    best_package.each do |package|
      count[package] += 1
    end
    count
  end

  def print_result
    # Calculate and display the order form to the user
    @display.calculate_order_summary
    @display.order_table
  end

  def display_error
    puts @error
  end

end
