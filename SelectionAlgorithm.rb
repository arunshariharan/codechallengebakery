class SelectionAlgorithm
  def initialize(quantity, package)
    @quantity = quantity
    @package = package
    @package_combinations, @final_combinations = [], []
  end

  def find_minimal_packaging
    permutations = find_permutations
    final_combinations = find_matching_combinations(permutations)

    # The combinations are ordered ascendingly, hence the first value in the array would
    # contain the minimal packaging required. 
    # So return it , if it is not empty in the first place.
    return final_combinations.empty? ? combination_error : final_combinations.first
  end

  # Find all possible combinations for the given package, that equals or slightly exceeds the given total quantity of order
  def find_permutations
    package_combinations = []
    i = 1

    until package_combinations.flatten(1).any? { | combo | combo.inject(:+) >= @quantity + @package.inject(:+) }
      package_combinations << @package.repeated_permutation(i).to_a
      i += 1
    end
    package_combinations
  end

  # From the list of permutations, find the combinations that equal the total ordered items
  def find_matching_combinations(package_combinations)
    final_combinations = []
    
    package_combinations.each { | combinations |
      combinations.each { | each_combo |
        sum_of_the_combination = each_combo.inject(:+)

        # if the sum equals target quantity, then add the combination to final_combinations array
        if sum_of_the_combination == @quantity
          final_combinations << each_combo
        end        
      }
    }
    final_combinations
  end

  private

  def combination_error
    "The target order cannot be met with the current package combinations. Please enter a valid items number"
  end
end
