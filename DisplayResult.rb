require 'terminal-table'
require_relative 'SelectionAlgorithm'

class DisplayResult

  attr_accessor :name, :price, :quantity, :pack, :total_quantity, :total_price

  def initialize
    @total_quantity, @total_price, @rows = 0, 0, []
  end

  # set values that will be used to display the table later on
  def set_values(pack, quantity, price, name)
    @total_quantity += quantity * pack
    @total_price += quantity * price
    @name, @price, @quantity, @pack = name, price, quantity, pack
    table_values = calculate_line_item_total_price
    add_data_to_table(table_values)
  end

  # Calculate values for line item
  def calculate_line_item_total_price
    { total_price: ((price * quantity).round(2)) }
  end

  # Add line item to the order summary
  def add_data_to_table(table_values)
    quant = quantity.to_s + ' x Pack of ' + pack.to_s
    @rows << [name, quant , '$' + price.to_s, '$' + table_values[:total_price].to_s]
  end

  # Calculate the total quantity and price
  def calculate_order_summary
    @rows << :separator
    @rows << ['Total',@total_quantity,'','$' + @total_price.round(2).to_s]
  end

  # Display the final order summary to the user
  def order_table
    table = Terminal::Table.new :title => "Ruby's Bakery" , :headings => ['Name of the item', 'Ordered Quantity', 'Individual Pack price', 'Total Price'], :rows => @rows
    table.align_column(0, :center)
    table.align_column(1, :center)
    table.align_column(2, :center)
    puts "\n\nThank you for ordering at Ruby's Bakery. Here is your order list.\n\n"
    puts table
  end
end



