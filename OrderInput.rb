require_relative 'ProcessOrder'
require_relative 'Catalogue'

class OrderInput
  def initialize
    @order_code, @items = "", ""
    @catalogue = Catalogue.new
  end

  def get_input
    # Start the execution of program
    puts "\n\n"
    puts "Enter the Code of the order:\n"
    @order_code = gets.chomp
    puts "\nEnter the number of items to order:\n"
    @items = gets.chomp
  end

  def display_catalogue
    catalogue.display_catalogue
  end

  def catalogue
    @catalogue
  end

  def process_order
    input = ProcessOrder.new(@order_code, @items, @catalogue)
  end
end

order = OrderInput.new
order.display_catalogue
order.get_input
order.process_order.execute