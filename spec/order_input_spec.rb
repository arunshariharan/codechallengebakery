require_relative '../Catalogue'
require_relative '../OrderInput'
require_relative '../ProcessOrder'
require 'terminal-table'

describe 'OrderInput' do 
  let(:order) { OrderInput.new }
  let(:cat) { Catalogue.new }
  
  context '#get_input' do
    subject { order.get_input }

    it 'asks the user to enter the code and items for the order' do
      expect { subject }.to output("\n\nEnter the Code of the order:\n\nEnter the number of items to order:\n").to_stdout
    end
  end

  context '#display_catalogue' do
    before do
      allow(order).to receive(:catalogue) { cat }
      allow(cat).to receive(:products) { "" }
      allow(cat).to receive(:display_catalogue) { "" }
    end

    it 'calls display_catalogue to display the products to user' do
      expect(cat).to receive(:display_catalogue).once
      order.display_catalogue
    end
  end

  context '#process_order' do
    let(:process) { ProcessOrder.new("VS5", "11", cat) }

    it 'calls ProcessOrder with the called parameters' do
      expect(process.order_code).to eql "VS5"
      expect(process.items).to eql "11"
    end
  end
end
