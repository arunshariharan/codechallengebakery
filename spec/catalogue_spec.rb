require 'terminal-table'
require_relative '../Catalogue'
require_relative '../OrderInput'

describe 'Catalogue' do
  let(:order) { OrderInput.new }
  let(:catalogue) { order.catalogue }

  context '#products' do
    it 'returns the correct product data from the database' do
      raw_products = File.read("products.json")
      test_products = JSON.parse(raw_products)
      expect(catalogue.products).to eql test_products
    end 
  end

  context '#display_catalogue' do
    subject { catalogue.display_catalogue }
    let(:products) do
      {
        "VS5" => {
          "name" => "Vegemite Scroll",
          "code" => "VS5",
          "packs" => {
            "3" => 6.99
          }
        },
      }
    end

    before do
      allow(catalogue).to receive(:products) { products }
    end
    
    it 'displays the correct catalogue' do
      expected = <<-EXPECTED
+---------------------+------+-------+------------+
|               Ruby's Bakery Menu                |
+---------------------+------+-------+------------+
| Name of the Product | Code | Packs | Pack Price |
+---------------------+------+-------+------------+
+---------------------+------+-------+------------+
| Vegemite Scroll     | VS5  |   3   |      $6.99 |
+---------------------+------+-------+------------+
      EXPECTED
      expect { subject }.to output(expected).to_stdout
    end    
  end
end