require_relative '../InputValidator'
require_relative '../Catalogue'
require_relative '../SelectionAlgorithm'
require_relative '../DisplayResult'
require_relative '../ProcessOrder'

describe 'Input' do 
  let(:order_code) { 'VS5' }
  let(:items) { "34" }
  let(:catalogue) { Catalogue.new }
  let!(:input) { ProcessOrder.new(order_code, items, catalogue) }
  let(:products) do
    {
      "VS5" => {
        "name" => "Vegemite Scroll",
        "code" => "VS5",
        "packs" => {
          "3" => 6.99,
          "5" => 8.99
        }
      },
    }
  end

  context '#execute' do
    subject { input.execute }

    context 'with invalid input' do
      context'for product' do
        let(:order_code) { 'LL' }
        it 'throws product related error' do
          subject
          expect(input.error). to eql "\nThe product does not exist. Please try again."
        end
      end

      context 'for items number' do
        let(:items) { '-1.54' }
        it 'throws product related error' do
          subject
          expect(input.error). to eql "\nPlease enter a valid item number."
        end
      end
    end

    context 'with valid inputs' do
      let(:display) { DisplayResult.new }
      before do
        allow(catalogue).to receive(:products) { products }
        allow(input).to receive(:display) { display }
      end

      it 'finds combinations' do
        expect(input).to receive(:find_combinations).once
        subject
      end

      it 'finalizes orders' do
        expect(input).to receive(:finalize_orders).once
        subject
      end

      it 'checks the package status' do
        expect(input).to receive(:check_package_status).twice
        subject
      end

      context 'when package status is false' do
        before do
          allow(input).to receive(:best_package) { "Error!" }
        end

        it 'shows error returned from selection algorithm to user' do
          subject
          expect(input.error).to eql "Error!"
        end
      end

      context 'when package status is true' do
        before do
          allow(input).to receive(:best_package) { [] }
        end

        it 'calculates order summary' do
          expect(display).to receive(:calculate_order_summary).once
          display.calculate_order_summary
        end

        it 'prints result to user' do
          expect(display).to receive(:order_table).once
          display.order_table
        end
      end
    end

    
  end

  context '#select_product' do
    before do
      allow(catalogue).to receive(:products) { products }
    end

    subject { input.select_product }

    it 'correctly assigns number of packs from the product' do
      subject
      expect(input.packs).to eql [3,5]
    end

    it 'correctly assigns prices of each pack' do
      subject
      expect(input.prices).to eql [6.99, 8.99]
    end
  end

  context '#find_combinations' do
    let(:items) { 11 }
    let(:packs) { [3,5] }
    subject { input.find_combinations }

    before do
      allow(input).to receive(:packs){ packs }
    end

    it 'initializes selection algorithm' do
      double(SelectionAlgorithm.new(items,[3,5]))
      subject
    end

    it 'calls the selection algorithm with items and packs' do
      expect(subject).to eql [3,3,5]
    end
  end

  context '#finalize_orders' do
    subject { input.finalize_orders }

    context 'when package is invalid' do
      let(:empty_line_item) { Hash.new(0) }

      it 'returns nil' do
        allow(input).to receive(:best_package) { "An Error Message" }
        expect(subject).to be_nil
      end

      it 'value of line item is not set and hence nil' do
        subject
        expect(input.line_item).to eql empty_line_item        
      end
    end

    context 'when package is valid' do
      let(:line_item_1) { [products["VS5"]["name"], "1 x Pack of 3", "$6.99", "$6.99"] }
      let(:line_item_2) { [products["VS5"]["name"], "1 x Pack of 5", "$8.99", "$8.99"] }

      before do
        allow(input).to receive(:best_package) { [3,5] }
      end

      it 'initializes new Display Result class' do
        double(DisplayResult.new)
        subject
      end

      it 'sets line item the values returned from DisplayResult class' do
        subject
        expect(input.line_item).to eql [line_item_1, line_item_2]
      end
    end

  end
end