require_relative '../SelectionAlgorithm'

describe 'SelectionAlgorithm' do
  let(:package) { [1] }
  let(:quantity) { 3 }
  let(:permutations) { [[[1]], [[1,1]], [[1,1,1]], [[1,1,1,1]]] }
  let(:final_combo) { [[5,3], [3,5]] }
  let(:combo) { SelectionAlgorithm.new(quantity, package) }

  context '#find_minimal_packaging' do
    subject { combo.find_minimal_packaging }

    it 'calls find_permutations method' do
      allow(combo).to receive(:find_matching_combinations) { [5,3] }
      expect(combo).to receive(:find_permutations)
      subject
    end

    it 'calls find_matching_combinations method' do
      allow(combo).to receive(:find_permutations) { permutations }
      expect(combo).to receive(:find_matching_combinations) { [5,3] }
      subject
    end

    context 'when a combination cannot be formed' do
      it 'should return combination error' do
        allow(combo).to receive(:find_permutations) { permutations }
        allow(combo).to receive(:find_matching_combinations) { [] }
        expect(subject).to eql "The target order cannot be met with the current package combinations. Please enter a valid items number"
      end
    end

    context 'when a combination is formed' do
      it 'returns the first combination' do
        allow(combo).to receive(:find_permutations) { permutations }
        allow(combo).to receive(:find_matching_combinations) { final_combo }
        expect(subject).to eql [5,3]
      end
    end
  end

  context '#find_permutations' do
    subject { combo.find_permutations }

    it 'returns all possible combinations for a given package and quantity' do
      expect(subject).to eql permutations
    end
  end

  context '#find_matching_combinations' do
    subject { combo.find_matching_combinations(permutations) }
    it 'returns all combinations that match the given quantity' do
      expect(subject).to eql [[1,1,1]]
    end

    context 'when a combination cannot be formed' do
      let(:quantity) { 0 }
      it 'returns an empty array' do
        expect(subject).to eql []
      end
    end
  end
end