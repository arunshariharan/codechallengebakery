require_relative '../InputValidator'
require_relative '../Catalogue'
require_relative '../OrderInput'

describe 'InputValidator' do
  let(:code) { "MB11" }
  let(:items) { "11" }
  let(:input) { OrderInput.new }
  let!(:input_validator) { InputValidator.new(code, items, input.catalogue) }

  context '#is_valid_number?' do
    subject { input_validator.is_valid_number? }

    context 'a positive integer' do
      it 'returns true' do
        expect(subject).to eql true
      end
    end

    context 'for zero as the input' do
      let(:items) { "0" }
      it 'returns true' do
        expect(subject).to eql true
      end
    end

    context 'for a negative value' do
      let(:items) { "-1" }
      it 'returns nil' do
        expect(subject).to eql nil
      end
    end

    context 'for a float value' do
      let(:items) { "1.45" }
      it 'when a float value is entered' do
        expect(subject).to eql false
      end
    end

    context 'for a non-number' do
      let(:items) { "stupid entry" }
      it 'when a non-number is entered' do
        expect(subject).to eql false
      end
    end
  end

  context '#check_existence' do
    subject { input_validator.check_existence }

    context 'for a code that is present in the system' do
      it 'returns true' do
        expect(subject).to eql true
      end
    end

    context 'for a code that is not present in the system' do
      let(:code) { "A non-valid code here :P" }
      it 'returns false' do
        expect(subject).to eql false
      end
    end
  end

  context '#product_error' do
    subject{ input_validator.product_error }
    it 'returns error relating to product' do
      expect(subject).to eql "\nThe product does not exist. Please try again."
    end
  end

  context '#items_error' do
    subject{ input_validator.items_error }
    it 'returns error relating to items' do
      expect(subject).to eql "\nPlease enter a valid item number."
    end
  end
end