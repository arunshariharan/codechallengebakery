require_relative '../DisplayResult'

describe 'DisplayResult' do
	let(:display) { DisplayResult.new }

  context '#set_values' do
    subject { display.set_values(5, 2, 6.99, 'Pizza') }
    it 'sets set_values for total_quantity, total_price and the params passed' do
      subject
      expect(display.name).to eql 'Pizza'
      expect(display.price).to eql 6.99
      expect(display.quantity).to eql 2
      expect(display.pack).to eql 5
      expect(display.total_price).to eql 13.98
      expect(display.total_quantity).to eql 10
    end

    it 'calls calculate_line_item_total_price' do
      expect(display).to receive(:calculate_line_item_total_price).once
      allow(display).to receive(:add_data_to_table)
      subject
    end

    it 'calls add_data_to_table method' do
      allow(display).to receive(:calculate_line_item_total_price)
      expect(display).to receive(:add_data_to_table).once
      subject
    end
  end

  context '#calculate_line_item_total_price' do
    let(:total_price) { { total_price: 146.79 } }
    subject { display.calculate_line_item_total_price }

    before do
      display.set_values(5, 21, 6.99, 'Pizza')
    end

    it 'returns total price, rounded to 2 values' do
      expect(subject).to eql total_price
    end
  end

  context '#add_data_to_table' do
    let(:total_price) { { total_price: 13.98 } }
    let(:row_1) { ["Pizza", "2 x Pack of 3", "$6.99", "$13.98"] }
    subject { display.add_data_to_table(total_price) }

    before do
      display.set_values(3, 2, 6.99, 'Pizza')
    end

    it 'returns an array that contains values set previously' do
      expect(subject).to eql [row_1, row_1]
    end
  end

  context '#calculate_order_summary' do
    let(:total_price) { { total_price: 13.98 } }
    let(:row_1) { ["Pizza", "2 x Pack of 3", "$6.99", "$13.98"] }
    let(:row_2) { ["Total", 6, "", "$13.98"] }
    subject { display.calculate_order_summary }

    before do
      display.set_values(3, 2, 6.99, 'Pizza')
    end

    it 'returns an array that contains values set previously' do
      expect(subject).to eql [row_1, :separator, row_2]
    end
  end

  context '#order_table' do
    let(:total_price) { { total_price: 13.98 } }
    subject { display.order_table }

    before do
      display.set_values(3, 2, 6.99, 'Pizza')
      display.calculate_order_summary
    end

    it 'displays table with all the values calculated previously' do
      expected = <<-EXPECTED


Thank you for ordering at Ruby's Bakery. Here is your order list.

+------------------+------------------+-----------------------+-------------+
|                               Ruby's Bakery                               |
+------------------+------------------+-----------------------+-------------+
| Name of the item | Ordered Quantity | Individual Pack price | Total Price |
+------------------+------------------+-----------------------+-------------+
|      Pizza       |  2 x Pack of 3   |         $6.99         | $13.98      |
+------------------+------------------+-----------------------+-------------+
|      Total       |        6         |                       | $13.98      |
+------------------+------------------+-----------------------+-------------+
      EXPECTED
      expect { subject }.to output(expected).to_stdout
    end
  end

end