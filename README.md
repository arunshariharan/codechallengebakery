# README #

This is my initial solution to the Code challenge question (find in Problem/ directory)

### What do you need to run this? ###

* Ruby (1.9 or greater)

### How to run? ###

* Clone the repository
* Navigate to the directory using the command line / terminal
* run using `ruby OrderInput.rb`

### What is due for next version update? ###

* Better tests
* Updated Project structure
* Updated Algorithm (to make it more efficient)